LOCAL_PATH := $(call my-dir)

## libfftw module
include $(CLEAR_VARS)

LOCAL_MODULE    := libfftw
LOCAL_CFLAGS    := -Werror -g \
        -I$(LOCAL_PATH)/fftw \
        -I$(LOCAL_PATH)/fftw/api \
        -I$(LOCAL_PATH)/fftw/kernel \
        -I$(LOCAL_PATH)/fftw/dft \
        -I$(LOCAL_PATH)/fftw/rdft \
        -I$(LOCAL_PATH)/fftw/reodft \
        -I$(LOCAL_PATH)/fftw/simd \
        -I$(LOCAL_PATH)/fftw/rdft/simd \
-I$(LOCAL_PATH)/fftw/rdft/scalar \
-I$(LOCAL_PATH)/fftw/dft/simd \
-I$(LOCAL_PATH)/fftw/dft/scalar

LOCAL_SRC_FILES := $(shell cd $(LOCAL_PATH); find ./fftw/ -type f -name '*.c'; find ./fftw/ -type f -name '*.cpp')

LOCAL_LDLIBS    := \
                   -llog -lm
                   
include $(BUILD_STATIC_LIBRARY)

## libaubio module
include $(CLEAR_VARS)

LOCAL_MODULE    := libaubio
LOCAL_CFLAGS    := -Werror -g \
        -I$(LOCAL_PATH)/aubio \
        -I$(LOCAL_PATH)/fftw \
        -I$(LOCAL_PATH)/include

 
LOCAL_SRC_FILES := $(shell cd $(LOCAL_PATH); find ./aubio/ -type f -name '*.c'; find ./aubio/ -type f -name '*.cpp')
LOCAL_STATIC_LIBRARIES := libfftw
LOCAL_LDLIBS    := \
                   -llog -lm -lfftw

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE   := speechandpitch
LOCAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_CFLAGS := -O3 \
		-Werror -g \
	    -I$(LOCAL_PATH)/aubio \
        -I$(LOCAL_PATH)/include

LOCAL_CPPFLAGS :=$(LOCAL_CFLAGS)

LOCAL_SRC_FILES := speechandpitch.c \
java_interface_wrap.cpp \
opensl_io/opensl_io.c\
$(shell cd $(LOCAL_PATH); find ./aubio/ -type f -name '*.c'; find ./aubio/ -type f -name '*.cpp')

LOCAL_STATIC_LIBRARIES :=libfftw libaubio 
LOCAL_LDLIBS := \
				-llog -lOpenSLES -llog -lm

include $(BUILD_SHARED_LIBRARY)