LOCAL_PATH := $(call my-dir)

## libaubio module
include $(CLEAR_VARS)

LOCAL_MODULE    := libaubio
LOCAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_CFLAGS    := -Werror -g \
        -I$(LOCAL_PATH)/aubio \
        -I$(LOCAL_PATH)/fftw \
        -I$(LOCAL_PATH)/include
        

 
LOCAL_SRC_FILES := $(shell cd $(LOCAL_PATH); find ./aubio/ -type f -name '*.c'; find ./aubio/ -type f -name '*.cpp')
#LOCAL_STATIC_LIBRARIES := libfftw
LOCAL_LDLIBS    := \
                   -llog -lm 

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
 
LOCAL_MODULE   := speechandpitch
LOCAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_CFLAGS := -O3 \
		-Werror -g \
	    -I$(LOCAL_PATH)/aubio \
        -I$(LOCAL_PATH)/include

LOCAL_CPPFLAGS :=$(LOCAL_CFLAGS)
 
LOCAL_SRC_FILES := speechandpitch.cpp \
java_interface_wrap.cpp \
opensl_io/opensl_io.c \
monowav/monowav.cpp \
$(shell cd $(LOCAL_PATH); find ./aubio/ -type f -name '*.c'; find ./aubio/ -type f -name '*.cpp')

LOCAL_STATIC_LIBRARIES := libaubio 
LOCAL_LDLIBS := \
				-llog -lOpenSLES -llog -lm

include $(BUILD_SHARED_LIBRARY)