#define AUBIO_UNSTABLE 1

#ifdef __cplusplus
extern "C" {
#endif

#include <android/log.h>
#include "opensl_io/opensl_io.h"
#include "monowav/monowav.h"
#include <aubio.h>
#include <stdio.h>



#define BUFFERFRAMES 1024
#define SR 44100
#define LOGPRINT __android_log_print
static int recorder_is_on;
smpl_t pitch_pool[10];
FILE * pitchresult ;

void start_process()
{
	fvec_t * input_buffer;
	fvec_t * pitch;
//	fvec_t * output_buffer;
	char pitch_method[]="yinfft";
	char pitch_unit[] ="midi";
	aubio_pitch_t *aubio_pointer;
	OPENSL_STREAM  *opensl_pointer;
	int buffer_size, i, j;
	float silence_threshold = -20.0;
	float pitch_tolerance = 0.85;
	int pos_to_put_ans = 0; //postion to put the current answer
	float  inbuffer[BUFFERFRAMES];
	opensl_pointer = android_OpenAudioDevice(SR,1,2,BUFFERFRAMES);//opensl pointer initialize
//	FILE * audiofile = fopen("/sdcard/audiodata.txt","w+");
	pitchresult = fopen("/sdcard/pitchresult.txt","w+");
    MonoWav monowavfile = MonoWav("/sdcard/audio_recording.wav");
//    aubio_wavetable_t *wavetable;
//    wavetable = new_aubio_wavetable (SR, BUFFERFRAMES);
//    aubio_wavetable_play ( wavetable );

	if(NULL == opensl_pointer)
	{
			return; //return if the pointer did not successfully applied memory
	}
	else
	{ 	//aubio initialize
		input_buffer = new_fvec (BUFFERFRAMES);
		pitch = new_fvec (1);
		aubio_pointer  = new_aubio_pitch(pitch_method, BUFFERFRAMES ,BUFFERFRAMES , SR);// aubio pointer initialize
		if(NULL == aubio_pointer) return;
		aubio_pitch_set_tolerance (aubio_pointer, pitch_tolerance);
		aubio_pitch_set_silence (aubio_pointer, silence_threshold);
		aubio_pitch_set_unit (aubio_pointer, pitch_unit);

	}
	recorder_is_on = 1;

	int counter = 1;
	while(recorder_is_on)
	{
		buffer_size = android_AudioIn(opensl_pointer, inbuffer, BUFFERFRAMES);//read in audio data into inbuffer
		fvec_zeros(input_buffer);
		for(i = 0; i< buffer_size; i ++)
		{
		   input_buffer->data[i] = (inbuffer[i]);//let the aubio get data to use
		}

		input_buffer->length=buffer_size;
		//write file of inbuffer

		short int *wav_buffer = monowavfile.request_fill(buffer_size);
        for (i=0; i<buffer_size; i++)
        {
            wav_buffer[i] = 32768 * inbuffer[i];
        }


        aubio_pitch_do (aubio_pointer, input_buffer, pitch);//do the pitch detection part
		pitch_pool[pos_to_put_ans ++] = pitch->data[0];//result is in data[0]

//		smpl_t freq = fvec_get_sample(pitch, 0);
//		fvec_zeros(output_buffer);
//		aubio_wavetable_set_amp ( wavetable, aubio_level_lin (input_buffer));
//		aubio_wavetable_set_freq ( wavetable, freq );
//		aubio_wavetable_do (wavetable, output_buffer, output_buffer);

		if (pitchresult != NULL)
		{
			fprintf(pitchresult,"%f\n",pitch->data[0]);
			fflush(pitchresult);
		}
		pos_to_put_ans %= 10; // the pitch pool has 10 smpl_t's room
	}

    fclose(pitchresult);
	aubio_cleanup();
	del_fvec(input_buffer);
//	del_fvec(output_buffer);
//	del_aubio_wavetable (wavetable);
	del_fvec(pitch);
	del_aubio_pitch(aubio_pointer);
	android_CloseAudioDevice(opensl_pointer);

}

int pitch()
{
	if (0 == recorder_is_on)// start after the recorder is on
	{
		return 0;
	}
	float mean = 0.0;
	int i = 0;
	for (i = 0; i < 10; i++) //use this to make the answer smooth
	{
		mean += pitch_pool[i];
	}
	mean /= 10.0;
	return mean;
}

void stop_process()
{
	recorder_is_on = 0;
}

#ifdef __cplusplus
}
#endif
