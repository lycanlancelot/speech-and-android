

#ifdef __cplusplus
extern "C" {
#endif
  void start_process();
  void stop_process();
  int 	pitch();
#ifdef __cplusplus
};
#endif
