#!/bin/sh

export ANDROID_NDK_ROOT=$HOME/Documents/Android/android-ndk-r9b

rm -rf src/speechandpitch
mkdir -p src/speechandpitch

swig -java -package speechandpitch -includeall -verbose -outdir src/speechandpitch -c++ -I/usr/local/include -I/System/Library/Frameworks/JavaVM.framework/Headers -I./jni -o jni/java_interface_wrap.cpp opensl_example_interface.i

$ANDROID_NDK_ROOT/ndk-build TARGET_PLATFORM=android-19 V=1




