package com.example.pitch;

import android.content.Context;
import android.graphics.*;
import android.view.View;

public class GraphicsDemo extends View {
	private Paint paint;
	public GraphicsDemo(Context context){
		super(context);
		paint = new Paint();
		paint.setAntiAlias(true);
		this.setKeepScreenOn(true);
		paint.setColor(Color.RED);
		
	}
	
	@Override
	public void onDraw(Canvas canvas)
	{
		canvas.drawColor(Color.WHITE);//refresh background color
		Rect rect= new Rect(30,30,50,50); //L h and position of down right
		canvas.drawRect(rect, paint);
		RectF rectf = new RectF(70f,30f,90f,90f);
		canvas.drawArc(rectf, 0, 360, true, paint);
		canvas.drawCircle(150, 30, 20, paint);
		float [] points=new float[]{200f,10f,200f,40f,300f,30f,400f,70f};
		canvas.drawLines(points,paint);
		canvas.drawText("Lancelot",	230, 30, paint);
	}
}
