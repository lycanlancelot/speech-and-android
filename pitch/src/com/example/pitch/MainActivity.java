package com.example.pitch;

import speechandpitch.speechandpitch;


import android.os.Bundle;
import android.app.Activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import android.widget.TextView;

public class MainActivity extends Activity {
	
	private TextView tv;
	Thread thread;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv = (TextView)findViewById(R.id.textView1);
		Button startbutton = (Button) findViewById(R.id.button1);
		startbutton.setOnClickListener(mlistener);

	    Button endbutton = (Button) findViewById(R.id.button2);
	    endbutton.setOnClickListener(EndRecorder);
	    
	    
//		setContentView(new GraphicsDemo(this));
		
		
		
	}
	
	private OnClickListener mlistener = new OnClickListener()//  this is the demon thread to post data to the front-end 
	{
		public void onClick(View v)
	      {
			thread = new Thread() {
				public void run() {
					setPriority(Thread.MAX_PRIORITY);
					speechandpitch.start_process();
					
				}
			};
			thread.start();   

			Runnable runnable= new Runnable(){
	    	  @Override
	    	  public void run()
	    	  {
	    		 int i=1;
				while(i==1)
				{
					makeitslow();
					final int value=speechandpitch.pitch();
					
					tv.post( new Runnable()
					{
						@Override
						public void run()
						{
							tv.setText(Integer.toString(value));
						}
					});
				
				}
			
	    	  }
			};
			new Thread(runnable).start();
	      }
	};
	private OnClickListener EndRecorder = new OnClickListener()
	{
		public void onClick(View v)
		{
			speechandpitch.stop_process();
		}
	};
	private void makeitslow()
	{
		try
		{
			Thread.sleep(23);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}
    public void onDestroy(){
    	
    	super.onDestroy();
    	speechandpitch.stop_process();
    	try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	thread = null;
    	
    }
    
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}
	

//	public native String stringTestNdk();
//	public native String unimplementedstringTestNdk();
////	
//	static {
//		System.loadLibrary("pitchdetection");
//		//System.loadLibrary("fftw3");
//	}
}
